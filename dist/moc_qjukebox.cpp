/****************************************************************************
** Meta object code from reading C++ file 'qjukebox.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.11.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../src/qjukebox.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'qjukebox.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.11.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_QJukeBox_t {
    QByteArrayData data[25];
    char stringdata0[431];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_QJukeBox_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_QJukeBox_t qt_meta_stringdata_QJukeBox = {
    {
QT_MOC_LITERAL(0, 0, 8), // "QJukeBox"
QT_MOC_LITERAL(1, 9, 24), // "on_file_browser_expanded"
QT_MOC_LITERAL(2, 34, 0), // ""
QT_MOC_LITERAL(3, 35, 11), // "QModelIndex"
QT_MOC_LITERAL(4, 47, 5), // "index"
QT_MOC_LITERAL(5, 53, 25), // "on_file_browser_collapsed"
QT_MOC_LITERAL(6, 79, 13), // "addToPlaylist"
QT_MOC_LITERAL(7, 93, 18), // "removeFromPlaylist"
QT_MOC_LITERAL(8, 112, 8), // "playItem"
QT_MOC_LITERAL(9, 121, 12), // "listPlaylist"
QT_MOC_LITERAL(10, 134, 10), // "directPlay"
QT_MOC_LITERAL(11, 145, 26), // "on_MainMenu_currentChanged"
QT_MOC_LITERAL(12, 172, 14), // "updateProgress"
QT_MOC_LITERAL(13, 187, 3), // "val"
QT_MOC_LITERAL(14, 191, 17), // "updateMaxProgress"
QT_MOC_LITERAL(15, 209, 33), // "on_playlist_browser_doubleCli..."
QT_MOC_LITERAL(16, 243, 25), // "on_play_pause_btn_pressed"
QT_MOC_LITERAL(17, 269, 26), // "on_progressBar_sliderMoved"
QT_MOC_LITERAL(18, 296, 8), // "position"
QT_MOC_LITERAL(19, 305, 19), // "on_stop_btn_clicked"
QT_MOC_LITERAL(20, 325, 24), // "on_seek_left_btn_clicked"
QT_MOC_LITERAL(21, 350, 25), // "on_seek_right_btn_clicked"
QT_MOC_LITERAL(22, 376, 19), // "on_exit_btn_clicked"
QT_MOC_LITERAL(23, 396, 28), // "on_volumeSlider_valueChanged"
QT_MOC_LITERAL(24, 425, 5) // "value"

    },
    "QJukeBox\0on_file_browser_expanded\0\0"
    "QModelIndex\0index\0on_file_browser_collapsed\0"
    "addToPlaylist\0removeFromPlaylist\0"
    "playItem\0listPlaylist\0directPlay\0"
    "on_MainMenu_currentChanged\0updateProgress\0"
    "val\0updateMaxProgress\0"
    "on_playlist_browser_doubleClicked\0"
    "on_play_pause_btn_pressed\0"
    "on_progressBar_sliderMoved\0position\0"
    "on_stop_btn_clicked\0on_seek_left_btn_clicked\0"
    "on_seek_right_btn_clicked\0on_exit_btn_clicked\0"
    "on_volumeSlider_valueChanged\0value"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_QJukeBox[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      18,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    1,  104,    2, 0x08 /* Private */,
       5,    1,  107,    2, 0x08 /* Private */,
       6,    0,  110,    2, 0x08 /* Private */,
       7,    0,  111,    2, 0x08 /* Private */,
       8,    0,  112,    2, 0x08 /* Private */,
       9,    0,  113,    2, 0x08 /* Private */,
      10,    0,  114,    2, 0x08 /* Private */,
      11,    1,  115,    2, 0x08 /* Private */,
      12,    1,  118,    2, 0x08 /* Private */,
      14,    1,  121,    2, 0x08 /* Private */,
      15,    1,  124,    2, 0x08 /* Private */,
      16,    0,  127,    2, 0x08 /* Private */,
      17,    1,  128,    2, 0x08 /* Private */,
      19,    0,  131,    2, 0x08 /* Private */,
      20,    0,  132,    2, 0x08 /* Private */,
      21,    0,  133,    2, 0x08 /* Private */,
      22,    0,  134,    2, 0x08 /* Private */,
      23,    1,  135,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    4,
    QMetaType::Void, QMetaType::LongLong,   13,
    QMetaType::Void, QMetaType::LongLong,   13,
    QMetaType::Void, 0x80000000 | 3,    4,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   18,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   24,

       0        // eod
};

void QJukeBox::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        QJukeBox *_t = static_cast<QJukeBox *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_file_browser_expanded((*reinterpret_cast< const QModelIndex(*)>(_a[1]))); break;
        case 1: _t->on_file_browser_collapsed((*reinterpret_cast< const QModelIndex(*)>(_a[1]))); break;
        case 2: _t->addToPlaylist(); break;
        case 3: _t->removeFromPlaylist(); break;
        case 4: _t->playItem(); break;
        case 5: _t->listPlaylist(); break;
        case 6: _t->directPlay(); break;
        case 7: _t->on_MainMenu_currentChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 8: _t->updateProgress((*reinterpret_cast< qint64(*)>(_a[1]))); break;
        case 9: _t->updateMaxProgress((*reinterpret_cast< qint64(*)>(_a[1]))); break;
        case 10: _t->on_playlist_browser_doubleClicked((*reinterpret_cast< const QModelIndex(*)>(_a[1]))); break;
        case 11: _t->on_play_pause_btn_pressed(); break;
        case 12: _t->on_progressBar_sliderMoved((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 13: _t->on_stop_btn_clicked(); break;
        case 14: _t->on_seek_left_btn_clicked(); break;
        case 15: _t->on_seek_right_btn_clicked(); break;
        case 16: _t->on_exit_btn_clicked(); break;
        case 17: _t->on_volumeSlider_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject QJukeBox::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_QJukeBox.data,
      qt_meta_data_QJukeBox,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *QJukeBox::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *QJukeBox::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_QJukeBox.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int QJukeBox::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 18)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 18;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 18)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 18;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
