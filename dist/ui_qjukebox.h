/********************************************************************************
** Form generated from reading UI file 'qjukebox.ui'
**
** Created by: Qt User Interface Compiler version 5.11.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_QJUKEBOX_H
#define UI_QJUKEBOX_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QListView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTreeView>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_QJukeBox
{
public:
    QAction *actionAbout;
    QAction *actionSettings;
    QWidget *centralWidget;
    QTabWidget *MainMenu;
    QWidget *tab;
    QListView *playlist_browser;
    QWidget *tab_2;
    QTreeView *file_browser;
    QWidget *tab_3;
    QPushButton *play_pause_btn;
    QPushButton *seek_right_btn;
    QPushButton *settings_btn;
    QPushButton *exit_btn;
    QPushButton *stop_btn;
    QPushButton *seek_left_btn;
    QSlider *progressBar;
    QSlider *volumeSlider;
    QMenuBar *menuBar;
    QMenu *menuQJukeBox;

    void setupUi(QMainWindow *QJukeBox)
    {
        if (QJukeBox->objectName().isEmpty())
            QJukeBox->setObjectName(QStringLiteral("QJukeBox"));
        QJukeBox->setEnabled(true);
        QJukeBox->resize(851, 533);
        QJukeBox->setTabShape(QTabWidget::Triangular);
        actionAbout = new QAction(QJukeBox);
        actionAbout->setObjectName(QStringLiteral("actionAbout"));
        actionSettings = new QAction(QJukeBox);
        actionSettings->setObjectName(QStringLiteral("actionSettings"));
        centralWidget = new QWidget(QJukeBox);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        centralWidget->setEnabled(true);
        MainMenu = new QTabWidget(centralWidget);
        MainMenu->setObjectName(QStringLiteral("MainMenu"));
        MainMenu->setGeometry(QRect(0, 0, 851, 451));
        tab = new QWidget();
        tab->setObjectName(QStringLiteral("tab"));
        playlist_browser = new QListView(tab);
        playlist_browser->setObjectName(QStringLiteral("playlist_browser"));
        playlist_browser->setGeometry(QRect(0, 0, 851, 421));
        playlist_browser->setEditTriggers(QAbstractItemView::NoEditTriggers);
        MainMenu->addTab(tab, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QStringLiteral("tab_2"));
        file_browser = new QTreeView(tab_2);
        file_browser->setObjectName(QStringLiteral("file_browser"));
        file_browser->setGeometry(QRect(0, 0, 848, 422));
        file_browser->setEditTriggers(QAbstractItemView::NoEditTriggers);
        file_browser->setAlternatingRowColors(true);
        file_browser->setSelectionMode(QAbstractItemView::SingleSelection);
        file_browser->setSortingEnabled(true);
        file_browser->setAnimated(true);
        MainMenu->addTab(tab_2, QString());
        tab_3 = new QWidget();
        tab_3->setObjectName(QStringLiteral("tab_3"));
        MainMenu->addTab(tab_3, QString());
        play_pause_btn = new QPushButton(centralWidget);
        play_pause_btn->setObjectName(QStringLiteral("play_pause_btn"));
        play_pause_btn->setGeometry(QRect(10, 470, 80, 23));
        seek_right_btn = new QPushButton(centralWidget);
        seek_right_btn->setObjectName(QStringLiteral("seek_right_btn"));
        seek_right_btn->setGeometry(QRect(310, 470, 80, 23));
        settings_btn = new QPushButton(centralWidget);
        settings_btn->setObjectName(QStringLiteral("settings_btn"));
        settings_btn->setGeometry(QRect(410, 470, 80, 23));
        exit_btn = new QPushButton(centralWidget);
        exit_btn->setObjectName(QStringLiteral("exit_btn"));
        exit_btn->setGeometry(QRect(510, 470, 80, 23));
        stop_btn = new QPushButton(centralWidget);
        stop_btn->setObjectName(QStringLiteral("stop_btn"));
        stop_btn->setGeometry(QRect(110, 470, 80, 23));
        seek_left_btn = new QPushButton(centralWidget);
        seek_left_btn->setObjectName(QStringLiteral("seek_left_btn"));
        seek_left_btn->setGeometry(QRect(210, 470, 80, 23));
        progressBar = new QSlider(centralWidget);
        progressBar->setObjectName(QStringLiteral("progressBar"));
        progressBar->setGeometry(QRect(10, 450, 841, 17));
        progressBar->setOrientation(Qt::Horizontal);
        volumeSlider = new QSlider(centralWidget);
        volumeSlider->setObjectName(QStringLiteral("volumeSlider"));
        volumeSlider->setGeometry(QRect(610, 470, 231, 16));
        volumeSlider->setOrientation(Qt::Horizontal);
        QJukeBox->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(QJukeBox);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setEnabled(true);
        menuBar->setGeometry(QRect(0, 0, 851, 24));
        menuQJukeBox = new QMenu(menuBar);
        menuQJukeBox->setObjectName(QStringLiteral("menuQJukeBox"));
        QJukeBox->setMenuBar(menuBar);

        menuBar->addAction(menuQJukeBox->menuAction());
        menuQJukeBox->addAction(actionAbout);
        menuQJukeBox->addAction(actionSettings);

        retranslateUi(QJukeBox);

        MainMenu->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(QJukeBox);
    } // setupUi

    void retranslateUi(QMainWindow *QJukeBox)
    {
        QJukeBox->setWindowTitle(QApplication::translate("QJukeBox", "QJukeBox", nullptr));
        actionAbout->setText(QApplication::translate("QJukeBox", "About", nullptr));
        actionSettings->setText(QApplication::translate("QJukeBox", "Settings", nullptr));
        MainMenu->setTabText(MainMenu->indexOf(tab), QApplication::translate("QJukeBox", "Playlist", nullptr));
        MainMenu->setTabText(MainMenu->indexOf(tab_2), QApplication::translate("QJukeBox", "Files", nullptr));
        MainMenu->setTabText(MainMenu->indexOf(tab_3), QApplication::translate("QJukeBox", "Settings", nullptr));
        play_pause_btn->setText(QString());
        seek_right_btn->setText(QString());
        settings_btn->setText(QString());
        exit_btn->setText(QString());
        stop_btn->setText(QString());
        seek_left_btn->setText(QString());
        menuQJukeBox->setTitle(QApplication::translate("QJukeBox", "QJukeBox", nullptr));
    } // retranslateUi

};

namespace Ui {
    class QJukeBox: public Ui_QJukeBox {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_QJUKEBOX_H
