#ifndef QJUKEBOX_H
#define QJUKEBOX_H

#include <QMainWindow>
#include <QFileSystemModel>
#include <QStringListModel>
#include <QMediaPlayer>
#include <QFile>
#include <QDir>
#include <QDirModel>
#include <QtAwesome/QtAwesome.h>

namespace Ui {
  class QJukeBox;
}

class QJukeBox : public QMainWindow
{
    Q_OBJECT

  public:
    explicit QJukeBox(QWidget *parent = 0);
    QMediaPlayer *player;
    QMediaPlaylist *player_playlist;
    void progressBuf(int i);
    ~QJukeBox();

  private slots:
    void on_file_browser_expanded(const QModelIndex &index);

    void on_file_browser_collapsed(const QModelIndex &index);

    void addToPlaylist();

    void removeFromPlaylist();

    void playItem();

    void listPlaylist();

    void directPlay();

    void on_MainMenu_currentChanged(int index);

    //void on_play_pause_btn_clicked(bool checked);

    void updateProgress(qint64 val);

    void updateMaxProgress(qint64 val);

    void on_playlist_browser_doubleClicked(const QModelIndex &index);

    void on_play_pause_btn_pressed();

    void on_progressBar_sliderMoved(int position);

    void on_stop_btn_clicked();

    void on_seek_left_btn_clicked();

    void on_seek_right_btn_clicked();

    void on_exit_btn_clicked();

    void on_volumeSlider_valueChanged(int value);

  private:
    Ui::QJukeBox *ui;
    void prepareMenu( const QPoint & pos );
    bool fileExists(QString path);
    QFileSystemModel *FileExplorerModel = new QFileSystemModel();
    QStringListModel *PlaylistModel = new QStringListModel();
    QStringList playlist;
    QtAwesome *awesome;
};

#endif // QJUKEBOX_H
